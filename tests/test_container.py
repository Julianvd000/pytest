from unittest.mock import Mock
from app.container import Container
import pytest

from app.database.container_database import ContainerDatabase

@pytest.fixture
def container():
    # All setup code goes here
    return Container(5)


def test_can_add_item_to_container(container) -> None:
    container.add("schrift")
    assert container.__sizeof__() == 1


def test_when_adding_item_then_item_is_in_container(container) -> None:
    container.add("schrift")
    assert "schrift" in container.get_items()


def test_when_add_more_then_max_items_should_fail(container):
    for _ in range(5):
        container.add("schrift")
    with pytest.raises(OverflowError):
        container.add("schrift")


def test_can_get_total_size_of_container(container):
    container.add("schrift")
    container.add("boek")
    container_database = ContainerDatabase()

    def mock_get_item(item:str):
        if item == "schrift":
            return 1
        if item == "boek":
            return 2

    container_database.get = Mock(side_effect=mock_get_item)
    assert container.get_total_content(container_database) == 3