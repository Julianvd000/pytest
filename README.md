[![pipeline status](https://gitlab.com/jointcyberrange.nl/pytest-julian/badges/main/pipeline.svg)](https://gitlab.com/jointcyberrange.nl/pytest-julian/-/commits/main)
[![coverage report](https://gitlab.com/jointcyberrange.nl/pytest-julian/badges/main/coverage.svg)](https://gitlab.com/jointcyberrange.nl/pytest-julian/-/commits/main)
[![Latest Release](https://gitlab.com/jointcyberrange.nl/pytest-julian/-/badges/release.svg)](https://gitlab.com/jointcyberrange.nl/pytest-julian/-/releases)

# PoC

Dit is een PoC voor het gebruik van Python en Pytest om een test te schrijven voor een Container class.

# User story
Learning story:
Als Student wil ik de workflow met een lokale hello world om begrijpen en kunnen uitvoeren van een unittest zodat ik hier van kan leren en laten zien aan andere.

requirements:
- 1 uitgewerkte minimale class:
- - Unittest
- - Pytest en Pytest.mock
- CI/CD met gitlab.yml


# Installatie
- Clone de repo
- pip install pytest


# Runnen
- open een terminal
- navigeer naar de root van de repo
- run: pytest

# Resultaat
- pytest zal de test uitvoeren en de resultaten tonen in de terminal:
```
collected 5 - items 
tests/test_container.py [80%]
tests/test_main.py [100%]
```

# Uitleg
- De test_container.py bevat de test voor de Container class
- De test_main.py bevat de test voor de main functie

# Uitbreiding
- coming soon.

# Draaien in een pipeline
Evventueel forken.

Dan Settings -> CI/CD General pipelines -> invullen .gitlab-ci.yml

Dan handmatig de pipeline draaien, of een nieuwe commit doen.

Wens: artifacts maken die je kunt downloaden.

