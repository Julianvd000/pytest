from app.container import Container


class ContainerDatabase:
    def __init__(self) -> None:
        pass

    def get(self, container: Container) -> Container:
        pass

    def add(self, container: Container) -> None:
        pass
