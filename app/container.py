from typing import List


class Container:
    def __init__(self, max_size: int) -> None:
        self.items: List[str] = []
        self.max_size = max_size
        pass

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def add(self, item: str):
        if self.__sizeof__() == self.max_size:
            raise OverflowError("Container is full")
        self.items.append(item)

    def remove(self, item):
        pass

    def get_items(self) -> List[str]:
        return self.items

    def __sizeof__(self) -> int:
        return len(self.items)

    def get_total_content(self, container_content) -> int:
        total_size_content = 0
        for item in self.items:
            total_size_content += container_content.get(item)
        return total_size_content